# Template Project for a REST Api Service for joel.muehlena.de

## Description

This is a template for a default REST API miscroservice in the joel.muehlena.de environment. It is based on node.js with express and TypeScript. Just clone it and build the service. Authentication and Authorization is handled by the API-Gateway. To now it is requered to to update the config of the API-Gateway manually and then restart it.
