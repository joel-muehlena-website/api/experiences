import mongoose from "mongoose";

export class MongoDBConnection {
  private mongoUri = `mongodb+srv://${process.env.MONGOUSERNAME}:${process.env.MONGODBPASSWORD}@${process.env.MONGOCONNECTIONSTRING}`;
  //private mongoUri = `mongodb://${process.env.MONGOUSERNAME}:${process.env.MONGODBPASSWORD}@${process.env.MONGOCONNECTIONSTRING}`;

  private mongoOptions = {
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  };

  connectToMongoDB = () => {
    mongoose
      .connect(this.mongoUri, this.mongoOptions)
      .then((_) => console.log("Connected to MongoDB Atlas Cluster"))
      .catch((err) => console.log(err));
  };
}
