export interface Experience {
  companyName: string;
  companyLocation: string;
  type: number;
  title: string;
  from: Date;
  to: Date;
  current: boolean;
  description: string;
  createdAt: Date;
  updatedAt: Date;
}
