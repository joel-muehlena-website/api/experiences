import { Router } from "express";
import { createExperience } from "../../common/v1/createExperience";
import { deleteExperience } from "../../common/v1/deleteExperience";
import { editExperience } from "../../common/v1/editExperience";
import {
  getExperience,
  getExperienceById,
} from "../../common/v1/getExperienceEntries";

export const experienceV1Router = Router();

experienceV1Router.get("/experience", getExperience);
experienceV1Router.get("/experience/:id", getExperienceById);
experienceV1Router.post("/experience/", createExperience);
experienceV1Router.put("/experience/:id", editExperience);
experienceV1Router.delete("/experience/:id", deleteExperience);
