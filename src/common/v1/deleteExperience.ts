import { NextFunction, Request, Response } from "express";

import { Experience as ExperienceModel } from "../../models/v1/Experiences";
import { Experience } from "../../models/v1/Experiences.interface";

export const deleteExperience = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const id = req.params.id;

  const experience: Experience = await ExperienceModel.findById(id);

  if (!experience) {
    return res.status(404).json({
      code: 404,
      msg: `No eduaction entry with the id ${id} found. Could not be edited.`,
    });
  }

  const delData: Experience = await ExperienceModel.findByIdAndDelete(id);

  if (delData) {
    res.status(200).json({ code: 200, msg: "Success", data: delData });
    return next();
  } else {
    res
      .status(500)
      .json({ code: 500, msg: "Failed to delete the experience entry" });

    return next();
  }
};
