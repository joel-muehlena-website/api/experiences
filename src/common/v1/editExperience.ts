import { NextFunction, Request, Response } from "express";

import { Experience as ExperienceModel } from "../../models/v1/Experiences";
import { Experience } from "../../models/v1/Experiences.interface";

export const editExperience = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const id = req.params.id;

  const experience: Experience = await ExperienceModel.findById(id);

  if (!experience) {
    res.status(404).json({
      code: 404,
      msg: `No experience entry with the id ${id} found. Could not be edited.`,
    });

    return next();
  }

  const {
    companyName,
    companyLocation,
    type,
    title,
    from,
    to,
    current,
    description,
  } = req.body;

  const updatedXp: any = {};
  if (companyName) updatedXp.companyName = companyName;
  if (companyLocation) updatedXp.companyLocation = companyLocation;
  if (type) updatedXp.type = type;
  if (title) updatedXp.title = title;
  if (description) updatedXp.description = description;
  if (from) updatedXp.from = from;
  if (to) updatedXp.to = to;
  if (current === true || current === false) updatedXp.current = current;
  updatedXp.updatedAt = new Date();

  const updatedXpData = await ExperienceModel.findByIdAndUpdate(
    id,
    { $set: updatedXp },
    { new: true }
  );

  if (updatedXpData) {
    res.status(200).json({ msg: 200, message: "Success", data: updatedXpData });

    return next();
  } else {
    res
      .status(500)
      .json({ code: 500, msg: "Failed to update the experience entry" });

    return next();
  }
};
