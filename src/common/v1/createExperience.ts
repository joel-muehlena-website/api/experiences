import { NextFunction, Request, Response } from "express";

import { Experience as ExperienceModel } from "../../models/v1/Experiences";
import { Experience } from "../../models/v1/Experiences.interface";

export const createExperience = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const {
    companyName,
    companyLocation,
    type,
    title,
    from,
    to,
    current,
    description,
  } = req.body;

  const date = new Date();
  const newExperienceEntry: Experience = {
    companyName,
    companyLocation,
    type,
    title,
    from,
    to,
    current,
    description,
    createdAt: date,
    updatedAt: date,
  };

  const saveExperienceEntry = await new ExperienceModel(
    newExperienceEntry
  ).save();

  if (saveExperienceEntry) {
    res
      .status(200)
      .json({ msg: 200, message: "Success", data: newExperienceEntry });

    return next();
  } else {
    res
      .status(500)
      .json({ code: 500, msg: "Failed to create the experience entry" });

    return next();
  }
};
