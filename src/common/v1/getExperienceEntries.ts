import { NextFunction, Request, Response } from "express";

import { Experience as ExperienceModel } from "../../models/v1/Experiences";
import { Experience } from "../../models/v1/Experiences.interface";

export const getExperience = async (
  _: Request,
  res: Response,
  next: NextFunction
) => {
  const experience: Experience[] = await ExperienceModel.find().sort("from");

  if (experience.length <= 0) {
    res.status(404).json({ code: 404, msg: "No experience entry found" });
    return next();
  }

  res.json({ code: 200, msg: "Success", data: experience });
  return next();
};

export const getExperienceById = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const id = req.params.id;

  const experience: Experience = await ExperienceModel.findById(id);

  if (!experience) {
    res
      .status(404)
      .json({ code: 404, msg: `No experience entry with thw id ${id} found` });
    return next();
  }

  res.json({ code: 200, msg: "Success", data: experience });
  return next();
};
